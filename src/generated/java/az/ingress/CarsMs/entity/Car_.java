package az.ingress.CarsMs.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Car.class)
public abstract class Car_ {

	public static volatile SingularAttribute<Car, String> city;
	public static volatile SingularAttribute<Car, Integer> year;
	public static volatile SingularAttribute<Car, String> name;
	public static volatile SingularAttribute<Car, Long> id;
	public static volatile SingularAttribute<Car, String> brand;

	public static final String CITY = "city";
	public static final String YEAR = "year";
	public static final String NAME = "name";
	public static final String ID = "id";
	public static final String BRAND = "brand";

}

