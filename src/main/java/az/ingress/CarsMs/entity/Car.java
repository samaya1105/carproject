package az.ingress.CarsMs.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cars")
@NamedQuery(name = "Car.findByBrand", query = "SELECT c FROM Car c where LOWER(c.brand) = lower(?1)")
public class Car{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String city;

    private String brand;

    @Column(nullable = false, unique = true)
    private Integer year;
}
