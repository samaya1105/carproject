package az.ingress.CarsMs.repository;

import az.ingress.CarsMs.dto.CarRequest;
import az.ingress.CarsMs.dto.CarResponse;
import az.ingress.CarsMs.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CarRepository extends JpaRepository<Car, Long>, JpaSpecificationExecutor<Car> {


    @Query("SELECT c FROM Car c WHERE c.name LIKE  %?1%")
    List<Car> findByCustomerNameQuery(@Param("name") String name);

    @Query("SELECT c FROM Car c WHERE c.brand LIKE  %?1%")
    List<Car> findByCustomerBrandQuery(@Param("brand") String brand);

    @Query(nativeQuery = true, value = "SELECT * FROM cars c WHERE c.brand <= ?1")
    List<Car> findByCustomerBrandNativeQuery(String brand);
}
