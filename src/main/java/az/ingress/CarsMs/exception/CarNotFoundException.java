package az.ingress.CarsMs.exception;

public class CarNotFoundException extends RuntimeException {

    private static final String MESSAGE = "Car ID %s Not Found ";

    public CarNotFoundException(Long id) {
        super(String.format(MESSAGE, id));
    }
}
