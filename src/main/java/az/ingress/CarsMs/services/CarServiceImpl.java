package az.ingress.CarsMs.services;

import az.ingress.CarsMs.dto.CarRequest;
import az.ingress.CarsMs.dto.CarResponse;
import az.ingress.CarsMs.entity.Car;
import az.ingress.CarsMs.exception.CarNotFoundException;
import az.ingress.CarsMs.repository.CarRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;
    private final ModelMapper modelMapper;

    public CarServiceImpl(CarRepository carRepository, ModelMapper modelMapper) {
        this.carRepository = carRepository;
        this.modelMapper = modelMapper;
    }

    void checkIfCarExists(Long id) {
        boolean existsById = carRepository.existsById(id);
        if (!existsById)
            throw new CarNotFoundException(id);
    }

    @Override
    public CarResponse create(CarRequest dto) {
        return modelMapper.map
                (carRepository.save
                        (modelMapper.map(dto, Car.class)), CarResponse.class);
    }

    @Override
    public CarResponse update(Long id, CarRequest dto) {
        return carRepository.findById(id)
                .map(car -> {
                    car = modelMapper.map(dto, Car.class);
                    car.setId(id);
                    return modelMapper.map(carRepository.save(car), CarResponse.class);
                }).orElseThrow(() -> new CarNotFoundException(id));
    }

    @Override
    public void delete(Long id) {
        checkIfCarExists(id);
        carRepository.deleteById(id);
    }

    @Override
    public CarResponse findById(Long id) {
        return carRepository.findById(id)
                .map(car -> modelMapper
                        .map(car, CarResponse.class))
                .orElseThrow(() -> new CarNotFoundException(id));
    }

    @Override
    public List<CarResponse> findAll() {
        return carRepository.findAll()
                .stream()
                .map(car -> modelMapper
                        .map(car, CarResponse.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<CarResponse> findBrandNativeQuery(String brand) {
        return carRepository.findByCustomerBrandNativeQuery(brand)
                .stream()
                .map(brands -> modelMapper.map(brands, CarResponse.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<CarResponse> findByName(String name) {
        return carRepository.findByCustomerNameQuery(name)
                .stream()
                .map(sum -> modelMapper.map(sum, CarResponse.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<CarResponse> findByBrand(String brand) {
        return carRepository.findByCustomerBrandQuery(brand)
                .stream()
                .map(fly -> modelMapper.map(fly, CarResponse.class))
                .collect(Collectors.toList());
    }

}
