package az.ingress.CarsMs.services;


import az.ingress.CarsMs.dto.CarRequest;
import az.ingress.CarsMs.dto.CarResponse;

import java.util.List;


public interface CarService {

    CarResponse create(CarRequest dto);
    CarResponse findById(Long id);
    List<CarResponse> findAll();
    List<CarResponse>findBrandNativeQuery(String brand);
    CarResponse update(Long id, CarRequest dto);
    List<CarResponse> findByName(String name);
    List<CarResponse> findByBrand(String brand);
    void delete(Long id);





}
