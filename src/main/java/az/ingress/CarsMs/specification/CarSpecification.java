package az.ingress.CarsMs.specification;

import az.ingress.CarsMs.entity.Car;
import az.ingress.CarsMs.entity.Car_;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CarSpecification {

    private final EntityManager entityManager;

    public CarSpecification(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Car> specification(String name
            , String city
            , String brand
            , String andOr
            , Integer year) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Car> cq = criteriaBuilder.createQuery(Car.class);
        Root<Car> carRoot = cq.from(Car.class);
        List<Predicate> predicatesList = new ArrayList<>();

        if (name != null) {
            predicatesList.add(criteriaBuilder.equal(carRoot.get(Car_.NAME), name));
        }
        if (city != null) {
            predicatesList.add(criteriaBuilder.equal(carRoot.get(Car_.CITY), city));
        }
        if (brand != null) {
            predicatesList.add(criteriaBuilder.equal(carRoot.get(Car_.BRAND), brand));
        }
        if (year != null) {
            predicatesList.add(criteriaBuilder.equal(carRoot.get(Car_.YEAR), year));
        }
        if (andOr.equalsIgnoreCase("and"))
            cq.where(criteriaBuilder
                    .and(predicatesList.toArray(new Predicate[0])));

        if (andOr.equalsIgnoreCase("or"))
            cq.where(criteriaBuilder
                    .or(predicatesList.toArray(new Predicate[0])));

        return entityManager.createQuery(cq).getResultList();
    }
}
