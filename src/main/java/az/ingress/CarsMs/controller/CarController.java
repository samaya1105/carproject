package az.ingress.CarsMs.controller;

import az.ingress.CarsMs.dto.CarRequest;
import az.ingress.CarsMs.dto.CarResponse;
import az.ingress.CarsMs.services.CarService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/v1/cars")
public class CarController {

    private final CarService carService;

    public CarController(CarService carService) {
        this.carService = carService;
    }


    @PostMapping
    public ResponseEntity<CarRequest> create(@RequestBody CarRequest dto) {
        log.info("Create cars body {}", dto);
        carService.create(dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(dto);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CarRequest> update(@PathVariable Long id
            , @RequestBody CarRequest dto) {
        log.info("Update cars by id {} body {}", id, dto);
        carService.update(id, dto);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(dto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<CarResponse> delete(@PathVariable Long id) {
        log.info("Delete cars by id {}", id);
        carService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<CarResponse> findById(@PathVariable Long id) {
        log.info("Get cars by id {}", id);
        return ResponseEntity.ok(carService.findById(id));
    }

    @GetMapping
    public ResponseEntity<List<CarResponse>> findAll() {
        log.info("Get cars all");
        return ResponseEntity.ok(carService.findAll());
    }

    @GetMapping("/names/{name}")
    public ResponseEntity<List<CarResponse>> findByName(@PathVariable String name) {
        log.info("Get cars name {}", name);
        return ResponseEntity.ok(carService.findByName(name));
    }

//    @GetMapping("/cities/{city}")
//    public ResponseEntity<List<CarResponse>> findByCity(@PathVariable String city) {
//        log.info("Get cars city {}", city);
//        return ResponseEntity.ok(carService.findByCity(city));
//    }

    @GetMapping("/brands/{brand}")
    public ResponseEntity<List<CarResponse>> findByBrand(@PathVariable String brand) {
        log.info("Get cars brand {}", brand);
        return ResponseEntity.ok(carService.findByBrand(brand));
    }

//    @GetMapping("/years/{year}")
//    public ResponseEntity<CarResponse> findByYear(@PathVariable Integer year) {
//        log.info("Get cars year {}", year);
//        return ResponseEntity.ok(carService.findByYear(year));
//    }

}
