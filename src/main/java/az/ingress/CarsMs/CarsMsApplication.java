package az.ingress.CarsMs;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarsMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarsMsApplication.class, args);
	}

}
