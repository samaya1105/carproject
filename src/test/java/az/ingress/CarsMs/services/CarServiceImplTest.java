package az.ingress.CarsMs.services;

import az.ingress.CarsMs.dto.CarRequest;
import az.ingress.CarsMs.dto.CarResponse;
import az.ingress.CarsMs.entity.Car;
import az.ingress.CarsMs.exception.CarNotFoundException;
import az.ingress.CarsMs.repository.CarRepository;
import az.ingress.CarsMs.specification.CarSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CarServiceImplTest {


    private static final Long DUMMY_ID = 1L;
    private static final String NAME = "Toyota";
    private static final String CITY = "Baku";
    private static final String BRAND = "Prius";
    private static final Integer YEAR = 2019;


    @InjectMocks
    private CarServiceImpl carServiceImpl;

    @Mock
    private CarRepository carRepository;

    @Spy
    private ModelMapper modelMapper;


    private Car car;
    private CarResponse carResponse;
    private CarRequest carRequest;
    private final List<CarResponse> carResponses = new ArrayList<>();
    private final List<Car> carList = new ArrayList<>();


    @BeforeEach
    void setUp() {

        car = Car.builder()
                .id(DUMMY_ID)
                .name(NAME)
                .city(CITY)
                .brand(BRAND)
                .year(YEAR)
                .build();

        carList.add(car);

        carResponse = CarResponse.builder()
                .id(DUMMY_ID)
                .name(NAME)
                .city(CITY)
                .brand(BRAND)
                .year(YEAR)
                .build();

        carRequest = CarRequest.builder()
                .name(NAME)
                .city(CITY)
                .brand(BRAND)
                .year(YEAR)
                .build();
    }

    @Test
    void update(){

        //Arrange
        when(carRepository.findById(DUMMY_ID)).thenReturn(Optional.of(car));
        when(carRepository.save(car)).thenReturn(car);

        //Act
        CarResponse carResponses = carServiceImpl.update(DUMMY_ID, carRequest);

        //verify
        verify(carRepository, times(1)).findById(DUMMY_ID);
        verify(carRepository, times(1)).save(car);

        //Assert
        assertThat(carResponses).isEqualTo(carResponses);
    }

    @Test
    void create() {
        Car cars = modelMapper.map(carRequest, Car.class);

        //Arrange
        when(carRepository.save(cars)).thenReturn(car);

        //Act
        CarResponse resultResponse = carServiceImpl.create(carRequest);

        //verify
        verify(carRepository, times(1)).save(cars);

        //Assert
        assertThat(resultResponse).isEqualTo(carResponse);
    }
    @Test
    void delete() {

        //Arrange
        when(carRepository.existsById(DUMMY_ID)).thenReturn(true);
        carServiceImpl.delete(DUMMY_ID);

        //verify
        verify(carRepository, times(1)).deleteById(DUMMY_ID);
    }

    @Test
    void findAll() {

        //Arrange
        when(carRepository.findAll()).thenReturn(carList);

        List<CarResponse> carResponses = carServiceImpl.findAll();

        //verify
        verify(carRepository, times(1)).findAll();

        //Assert
        assertThat(carResponses).isEqualTo(carResponses);
    }

    @Test
    void findById() {

        //Arrange
        when(carRepository.findById(DUMMY_ID)).thenReturn(Optional.of(car));

        CarResponse carResponse = carServiceImpl.findById(DUMMY_ID);

        //verify
        verify(carRepository, times(1)).findById(DUMMY_ID);

        //Assert
        assertThat(carResponse).isEqualTo(carResponse);
    }

    @Test
    void givenInvalidCarIdWhenUpdateThenException() {
        //Arrange
        when(carRepository.findById(DUMMY_ID)).thenReturn(Optional.empty());

        //Act & Assert
        assertThatThrownBy(() -> carServiceImpl
                .update(DUMMY_ID, carRequest))
                .isInstanceOf(CarNotFoundException.class);
    }
    @Test
    void givenInValidCarIdWhenDeleteThenException() {

        //Arrange
        when(carRepository.existsById(DUMMY_ID)).thenReturn(false);

        //Act & Assert
        assertThatThrownBy(() -> carServiceImpl.delete(DUMMY_ID))
                .isInstanceOf(CarNotFoundException.class);
    }

    @Test
    void givenInvalidCategoryIdWhenException() {

        //Arrange
        when(carRepository.findById(DUMMY_ID)).thenReturn(Optional.empty());

        //Act & Assert
        assertThatThrownBy(() -> carServiceImpl.findById(DUMMY_ID))
                .isInstanceOf(CarNotFoundException.class);
    }

    @Test
    void findBrandNativeQuery(){

        //Arrange
        when(carRepository.findByCustomerBrandNativeQuery(BRAND)).thenReturn(carList);

        List<CarResponse> carResponses = carServiceImpl.findBrandNativeQuery(BRAND);

        //verify
        verify(carRepository, times(1)).findByCustomerBrandNativeQuery(BRAND);


        //Assert
        assertThat(carResponses).isEqualTo(carResponses);
    }
}