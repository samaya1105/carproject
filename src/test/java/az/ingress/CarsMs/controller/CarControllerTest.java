package az.ingress.CarsMs.controller;

import az.ingress.CarsMs.dto.CarRequest;
import az.ingress.CarsMs.dto.CarResponse;
import az.ingress.CarsMs.services.CarService;
import com.fasterxml.jackson.databind.ObjectMapper;
import liquibase.pro.packaged.C;
import net.bytebuddy.agent.builder.AgentBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(CarController.class)
class CarControllerTest {

    private static final String MAIN_URL = "/v1/cars";
    private static final Long DUMMY_ID = 1L;
    private static final String NAME = "Toyota";
    private static final String CITY = "Baku";
    private static final String BRAND = "Prius";
    private static final Integer YEAR = 2019;


    @MockBean
    private CarService carService;

    @Autowired
    private MockMvc mockMvc;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private CarRequest carRequest;
    private CarResponse carResponse;
    private final List<CarResponse> carResponses = new ArrayList<>();


    @BeforeEach
    void setUp() {
        carResponse = CarResponse
                .builder()
                .id(DUMMY_ID)
                .name(NAME)
                .city(CITY)
                .brand(BRAND)
                .year(YEAR)
                .build();

        carResponses.add(carResponse);

        carRequest = CarRequest.builder()
                .name(NAME)
                .city(CITY)
                .brand(BRAND)
                .year(YEAR)
                .build();
    }

    @Test
    void update() throws Exception {

        //Arrange
        when(carService.update(DUMMY_ID, carRequest)).thenReturn(carResponse);

        //Act
        mockMvc.perform(MockMvcRequestBuilders.put(MAIN_URL + '/' + DUMMY_ID)
                        .content(objectMapper.writeValueAsString(carRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isAccepted())
                .andDo(print());

        //verify
        verify(carService, times(1)).update(DUMMY_ID, carRequest);

    }

    @Test
    void create() throws Exception {
        //Arrange
        when(carService.create(carRequest)).thenReturn(carResponse);

        //Act
        mockMvc.perform(MockMvcRequestBuilders.post(MAIN_URL)
                        .content(objectMapper.writeValueAsString(carRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());


        //verify
        verify(carService, times(1)).create(carRequest);
    }

    @Test
    void findAll() throws Exception {
        //Arrange
        when(carService.findAll()).thenReturn(carResponses);

        //Act
        mockMvc.perform(MockMvcRequestBuilders.get(MAIN_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(carResponses)));
        //verify
        verify(carService, times(1)).findAll();
    }

    @Test
    void delete() throws Exception {
        //Action
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders
                .delete(MAIN_URL + '/' + DUMMY_ID)
                .contentType(MediaType.APPLICATION_JSON));

        //Assert
        verify(carService, times(1)).delete(DUMMY_ID);
        resultActions.andExpect(status().isNoContent());
    }

    @Test
    void findByName() throws Exception {


        //Arrange
        when(carService.findByName(NAME)).thenReturn(carResponses);

        //Act
        mockMvc.perform(MockMvcRequestBuilders.get(MAIN_URL + "/names/" + NAME)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(carResponses)));

        //verify
        verify(carService, times(1)).findByName(NAME);
    }

    @Test
    void findById() throws Exception {

        //Arrange
        when(carService.findById(DUMMY_ID)).thenReturn(carResponse);

        //Act
        mockMvc.perform(MockMvcRequestBuilders.get(MAIN_URL + '/' + DUMMY_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(carResponse)));

        //verify
        verify(carService, times(1)).findById(DUMMY_ID);


    }

    @Test
    void findByBrand() throws Exception {

        //Arrange
        when(carService.findByBrand(BRAND)).thenReturn(carResponses);

        //Act
        mockMvc.perform(MockMvcRequestBuilders.get(MAIN_URL + "/brands/" + BRAND)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(carResponses)));

        //verify
        verify(carService, times(1)).findByBrand(BRAND);

    }
}